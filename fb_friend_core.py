import argparse
from threading import Thread
import datetime
import time
import sqlite3
import Facebook as fb
import ConfigParser
# output lib
# changed v1
from colorama import init
init(autoreset=True)
from colorama import Fore, Back, Style


def log(color, message):
	if color == 'y':
		return Style.BRIGHT + Fore.YELLOW + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'g':
		return Style.BRIGHT + Fore.GREEN + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'b':
		return Style.BRIGHT + Fore.BLUE + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'c':
		return Style.BRIGHT + Fore.CYAN + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'r':
		return Style.BRIGHT + Fore.RED + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'de':
		return Style.BRIGHT + Fore.RESET + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)

parser = argparse.ArgumentParser(description='FBOT Account Module')
parser.add_argument('-u', '-user')
parser.add_argument('-a', '-amount')
parser.add_argument('-d', '-delete')
args = parser.parse_args()

if args.d != None:
	conn = sqlite3.connect('db/jx.db')
	c = conn.cursor()
	c.execute('SELECT * from account WHERE email=?', (args.u,))
	r = c.fetchone()
	conn.close()
	uf = fb.Unfriender(
		amount = int(args.a),
		username = r[0],
		password = r[1],
		proxy_ip = r[3],
		proxy_port = r[4],
		)
	uf.start()

