import Facebook as fb
import time
from threading import Thread
import argparse
# check config
from colorama import init
init(autoreset=True)
from colorama import Fore, Back, Style
import datetime
import sqlite3
import requests
from requests.adapters import HTTPAdapter

parser = argparse.ArgumentParser(description='FBOT Account Module')
parser.add_argument('-d', '-delay')
args = parser.parse_args()


def log(color, message):
	if color == 'y':
		return Style.BRIGHT + Fore.YELLOW + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'g':
		return Style.BRIGHT + Fore.GREEN + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'b':
		return Style.BRIGHT + Fore.BLUE + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'c':
		return Style.BRIGHT + Fore.CYAN + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'r':
		return Style.BRIGHT + Fore.RED + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'de':
		return Style.BRIGHT + Fore.RESET + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
acc_list = []

conn = sqlite3.connect('db/jx.db')
c = conn.cursor()
c.execute('SELECT * from account WHERE active=1')
rows = c.fetchall()
conn.close()
for r in rows:
	acc_dict = {}
	acc_dict['username'] = r[0]
	acc_dict['proxy_ip'] = r[3]
	acc_dict['proxy_port'] = r[4]
	acc_list.append(acc_dict)
THREADS = len(acc_list)

def main():
	manager = ThreadManager()
	print log('g', 'Starting "AUTO ACCEPT MODULE" with {0} threads...'.format(THREADS))
	manager.start(int(THREADS))
class ThreadManager:
	def __init__(self):
		pass
		
	def start(self, threads):
		thread_refs = []
		for i in range(threads):
			t = MyThread(i)
			t.daemon = True
			t.start()
			time.sleep(1)
		for t in thread_refs:
			t.join()
class MyThread(Thread):
	def __init__(self, i):
		Thread.__init__(self)
		self.i = i
		
	def run(self):
		i = self.i
		user = acc_list[i]
		acc_it = fb.AutoAccept(user = user['username'], accept_delay = args.d)
		acc_it.start()

main()
while True:
	time.sleep(100)