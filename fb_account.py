import argparse
from threading import Thread
import time
import sqlite3
from Facebook.user import User
import tools


parser = argparse.ArgumentParser(description='FBOT Account Module')
parser.add_argument('-u', '-user')
parser.add_argument('-p', '-password')
parser.add_argument('-proxy')
parser.add_argument('-file')
args = parser.parse_args()

if args.file != None:
	accs_file = open(args.file, 'r')
	accs_list = accs_file.readlines()
	accs_file.close()

	THREADS = len(accs_list)

	def _main():
		manager = ThreadManager()
		print tools.log('g', 'Starting "ACCOUNT MODULE" with {0} threads...'.format(THREADS))
		manager.start(int(THREADS))
	class ThreadManager:
		def __init__(self):
			pass
			
		def start(self, threads):
			thread_refs = []
			for i in range(threads):
				t = MyThread(i)
				t.daemon = True
				t.start()
				time.sleep(1)
			for t in thread_refs:
				t.join()
	class MyThread(Thread):
		def __init__(self, i):
			Thread.__init__(self)
			self.i = i
			
		def run(self):
			i = self.i
			current_line = accs_list[i]
			cl = current_line.split(':')
			cl = {
			'username' : cl[0],
			'password' : cl[1],
			'proxy_ip' : cl[2],
			'proxy_port' : cl[3].split('\n')[0]
			}
			user = User(cl, add = True, max = THREADS, thread_no = i)	
	_main()
	while True:
		time.sleep(2)
else:
	if args.u == 'checker_001':
		print tools.log('c', 'Checking accounts...')
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		c.execute('SELECT * from account')
		rows = c.fetchall()
		print tools.log('g', 'Found a total of {0} accounts in db.'.format(len(rows)))
		for r in rows:
			print tools.log('c', 'Checking account "{0}"...'.format(r[0]))
			user = User([r[0], r[1], r[3], r[4]], max = 1, thread_no = 1)
			if user.login() == 'SUCCESS_LOGIN':
				print tools.log('g', 'Account "{0}" alive!'.format(r[0]))
				print 'good_acc'
				c.execute('UPDATE account set active = 1 WHERE email=?', (r[0],))
				conn.commit()
			else:
				print tools.log('r', 'Account "{0}" failed to login.'.format(r[0]))
				print 'bad_acc'
				c.execute('UPDATE account set active = 0 WHERE email=?', (r[0],))
				conn.commit()
		conn.close()
		time.sleep(2)
	else:
		THREADS = 1
		def _main():
			manager = ThreadManager()
			print tools.log('g', 'Starting "ACCOUNT MODULE" with {0} threads...'.format(THREADS))
			manager.start(int(THREADS))
		class ThreadManager:
			def __init__(self):
				pass
				
			def start(self, threads):
				thread_refs = []
				for i in range(threads):
					t = MyThread(i)
					t.daemon = True
					t.start()
					time.sleep(1)
				for t in thread_refs:
					t.join()
		class MyThread(Thread):
			def __init__(self, i):
				Thread.__init__(self)
				self.i = i
				
			def run(self):
				i = self.i
				try:
					cl = {
					'username' : args.u,
					'password' : args.p,
					'proxy_ip' : args.proxy.split(':')[0],
					'proxy_port' : args.proxy.split(':')[1].split('\n')[0]
					}
					user = User(cl, add = True, max = THREADS, thread_no = i)
				except Exception as e:
					print tools.log('r', 'ERROR on ACC [ {0} ]: {1}'.format(args.u, str(e)))
					print tools.log('r', 'TASK_DONE')
		_main()
		while True:
			time.sleep(2)