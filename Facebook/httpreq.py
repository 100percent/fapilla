from bs4 import BeautifulSoup as BS
from sqlstuff import SexySQL
sq = SexySQL()
MOBILE_HOST = 'https://m.facebook.com'
from selenium import webdriver
from selenium.webdriver.common.proxy import *
import tools
import os
class Browser:
	def __init__(self, **kwargs):
		self.user = sq.get_account_info(kwargs['user'])
		self.s = tools.new_session()
		tools.set_session_proxy(self.s, '{0}:{1}'.format(self.user['proxy_ip'], self.user['proxy_port']))
		self.get_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive'
		}
		self.post_group_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive',
		'Content-Type': 'application/x-www-form-urlencoded'
		}
		self.proxies = '{0}:{1}'.format(self.user['proxy_ip'], self.user['proxy_port'])
		# self.login()

	def login(self):
		s = self.s
		s.headers = self.get_headers
		get_home = s.get(MOBILE_HOST, verify = False)
		soup = BS(get_home.content)
		login_data = {}
		for _input in soup.findAll('input', attrs={'type' : 'hidden'}):
			login_data[_input['name']] = _input['value']
		login_data['login'] = 'Login'
		login_data['email'] = self.user['username']
		login_data['pass'] = self.user['password']
		post_login = s.post('{0}/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&refid=8'.format(MOBILE_HOST), data = login_data, verify = False)
		if post_login.content.find('logout.php') != -1:
			# extract cookies
			cookie_list = []
			for ck in s.cookies:
				cookie_dict = {}
				cookie_dict['domain'] = ck.domain
				cookie_dict['name'] = ck.name
				cookie_dict['value'] = ck.value
				cookie_dict['expiry'] = ck.expires
				cookie_list.append(cookie_dict)
			chrome_options = webdriver.ChromeOptions()
			chrome_options.add_argument('--proxy-server={0}'.format(self.proxies))
			chromedriver = '/'
			os.environ["webdriver.chrome.driver"] = chromedriver
			browser = webdriver.Chrome(chrome_options = chrome_options)
			browser.get('http://m.facebook.com')
			for cl in cookie_list:
				browser.add_cookie(cl)
			browser.get('http://facebook.com')
		else:
			chrome_options = webdriver.ChromeOptions()
			chrome_options.add_argument('--proxy-server={0}'.format(self.proxies))
			chromedriver = '/'
			os.environ["webdriver.chrome.driver"] = chromedriver
			browser = webdriver.Chrome(chrome_options = chrome_options)
			browser.get('http://m.facebook.com')
