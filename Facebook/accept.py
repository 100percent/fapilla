import tools
from sqlstuff import SexySQL
sq = SexySQL()
from bs4 import BeautifulSoup as BS
MOBILE_HOST = 'https://m.facebook.com'

class AutoAccept:
	def __init__(self, **kwargs):
		self.user = kwargs['user']
		self._user = sq.get_account_info(self.user)
		self.delay = kwargs['accept_delay']
		self.s = tools.new_session()
		tools.set_session_proxy(self.s, '{0}:{1}'.format(self._user['proxy_ip'], self._user['proxy_port']))
		self.get_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive'
		}
		self.post_group_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive',
		'Content-Type': 'application/x-www-form-urlencoded'
		}


	def login(self):
		s = self.s
		s.headers = self.get_headers
		get_home = s.get(MOBILE_HOST, verify = False)
		soup = BS(get_home.content)
		login_data = {}
		for _input in soup.findAll('input', attrs={'type' : 'hidden'}):
			login_data[_input['name']] = _input['value']
		login_data['login'] = 'Login'
		login_data['email'] = self._user['username']
		login_data['pass'] = self._user['password']
		try:
			post_login = s.post('{0}/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&refid=8'.format(MOBILE_HOST), data = login_data, verify = False)
			if post_login.content.find('logout.php') != -1:
				# print log('g', 'Login success.')
				return 'SUCCESS_LOGIN'
			else:
				print log('r', 'Account "{0}" login failed.'.format(self._user['username']))
				return 'ERROR_LOGIN_FAILED'
		except:
			return 'ERROR_LOGIN_FAILED'

	def start(self):
		if self.login() == 'SUCCESS_LOGIN':
			while True:
				s = self.s
				s.headers = self.get_headers
				get_friend_home = s.get('{0}/friends/center/requests/'.format(MOBILE_HOST))
				soup = BS(get_friend_home.content)
				for a in soup.findAll('a'):
					if a['href'].find('notifications.php?confirm') != -1:
						get_accept_friend = s.get('{0}{1}'.format(MOBILE_HOST, a['href']))
						if get_accept_friend.content.find('equest accepted') != -1:
							print log('g', 'friend_accepted')
						break
				time.sleep(int(self.delay))