import sqlite3
import datetime

class SexySQL:
	def __init__(self):
		pass

	def add_account(self, **args):
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		c.execute('INSERT into account (email, password, active, facebookName, accountCreated, proxyIp, proxyPort, fbId) VALUES (?, ?, 1, ?, ?, ?, ?, ?)', (args['username'], args['password'], args['full_name'], datetime.datetime.now().strftime("%Y-%m-%d"), args['proxy_ip'], args['proxy_port'], args['fb_id']))
		conn.commit()
		conn.close()


	def update_group(self, **args):
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		c.execute('DELETE from groups where accountEmail=?', (args['username'],))
		conn.commit()
		c.executemany('INSERT into groups VALUES (?, ?, ?)', args['grouplist'])
		conn.commit()
		conn.close()

	def delete_chat_state(self, **args):
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		c.execute('DELETE from chatData where chatFrom=? and chatTo=?', (args['chat_from'], args['chat_to']))
		conn.commit()
		conn.close()

	def add_chat(self, **args):
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		c.execute('INSERT into chatData (chatFrom, chatTo) VALUES (?, ?)', (args['chat_from'], args['chat_to']))
		conn.commit()
		conn.close()

	def check_chat_state(self, **args):
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		c.execute('SELECT chatState from chatData where chatFrom=? and chatTo=?', (args['chat_from'], args['chat_to']))
		row = c.fetchone()
		conn.close()
		if row == None:
			return 4
		else:
			return row[0]

	def update_chat_state(self, **args):
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		# update info_table SET accepted_friends = accepted_friends + 1 WHERE username = self.username
		c.execute('UPDATE chatData SET chatState = ? WHERE chatTo=? and chatFrom=?', (args['state'],args['chat_to'], args['chat_from']))
		conn.commit()
		conn.close()

	def get_account_info(self, _user):
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		c.execute('SELECT * from account where email=?', (_user,))
		row = c.fetchone()
		conn.close()
		user = {}
		if row != None:
			user['username'] = row[0]
			user['password'] = row[1]
			user['active'] = row[2]
			user['proxy_ip'] = row[3]
			user['proxy_port'] = row[4]
			user['account_created'] = row[5]
			user['fb_name'] = row[6]
			user['fb_id'] = row[8]
			user['msg1'] = row[9]
			user['msg2'] = row[10]
			user['msg3'] = row[11]
			return user
		else:
			return 'NONE'

	def get_4char(self):
		conn = sqlite3.connect('db/jx.db')
		c = conn.cursor()
		c.execute('SELECT `4char` from `4chars`')
		row = c.fetchone()
		conn.close()
		return row[0]