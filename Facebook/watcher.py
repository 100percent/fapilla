from other import *
from sqlstuff import SexySQL
sq = SexySQL()
from unfriend import Unfriender
MOBILE_HOST = 'https://m.facebook.com'

class FriendCounter:
	def __init__(self, **kwargs):
		self.user = kwargs['user']
		self._user = sq.get_account_info(username = self.user)
		self.s = requests.session()
		set_session_proxy(self.s, '{0}:{1}'.format(self._user['proxy_ip'], self._user['proxy_port']))
		self.s.mount('http://', HTTPAdapter(max_retries=99))
		self.s.mount('https://', HTTPAdapter(max_retries=99))
		self.get_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive'
		}
		self.post_group_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive',
		'Content-Type': 'application/x-www-form-urlencoded'
		}

	def login(self):
		s = self.s
		s.headers = self.get_headers
		get_home = s.get(MOBILE_HOST, verify = False)
		soup = BS(get_home.content)
		login_data = {}
		for _input in soup.findAll('input', attrs={'type' : 'hidden'}):
			login_data[_input['name']] = _input['value']
		login_data['login'] = 'Login'
		login_data['email'] = self._user['username']
		login_data['pass'] = self._user['password']
		try:
			post_login = s.post('{0}/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&refid=8'.format(MOBILE_HOST), data = login_data, verify = False)
			if post_login.content.find('logout.php') != -1:
				# print log('g', 'Login success.')
				return 'SUCCESS_LOGIN'
			else:
				print log('r', 'Login failed.')
				return 'ERROR_LOGIN_FAILED'
		except:
			return 'ERROR_LOGIN_FAILED'


	def start(self, _max, _amount):
		if self.login() == 'SUCCESS_LOGIN':
			s = self.s
			s.headers = self.get_headers
			get_myprofile = s.get('{0}/me'.format(MOBILE_HOST), verify = False)
			profile_url = get_myprofile.url.replace('://m.', '://')
			profile_url = profile_url.split('?')[0]
			while True:
				get_friends = s.get('{0}/friends_all'.format(profile_url), verify = False)
				_c = 0
				_tf = 0
				for cma in get_friends.content.split('"'):
					if cma.find('tab_count') != -1:
						tf = get_friends.content.split('"')[_c + 1]
						tf = tf.replace(':', '')
						tf = tf.replace(',', '')
						_tf = int(tf)
						break
					_c = _c + 1
				if _tf != 0:
					if _tf >= int(_max):
						print log('c', 'Auto Unfriend invoked! User "{0}" has {1} friends...'.format(self._user['username'], _tf))
						# print 'auto_unfriend {0}'.format(self._user['username'])
						Unfriender(
							amount = int(_amount),
							username = self._user['username'],
							password = self._user['password'],
							proxy_ip = self._user['proxy_ip'],
							proxy_port = self._user['proxy_port'],
							).start()
				"""for h3 in soup.findAll('h3'):
					print str(h3)
					if h3.text.find('Friends (') != -1:
						total_friends = h3.text.split('(')[1].split(')')[0]
						total_friends = total_friends.replace(',', '')
						print total_friends
						if int(total_friends) >= _max:
							print log('c', 'Auto Unfriend invoked! User "{0}" has {1} friends...'.format(self._user['username'], total_friends))
							print 'auto_unfriend {0}'.format(self._user['username'])
						break"""
				time.sleep(6000)