# 0x000000
import os
import time
import random
import re
import string
from threading import Thread
from subprocess import call
import subprocess
import datetime
from collections import OrderedDict as OD
import sys
import ConfigParser
import uuid
import pytz

# output lib
from colorama import init
init(autoreset=True)
from colorama import Fore, Back, Style

# win32API processes
import psutil

# process images
import PIL
from PIL import Image
from PIL import ImageGrab

# web stuff
import requests
from requests.adapters import HTTPAdapter
from bs4 import BeautifulSoup as BS
import json

# headless browser
from selenium import webdriver
from selenium.webdriver.common.proxy import *
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# database
import mysql.connector

# public functions
def log(color, message):
	if color == 'y':
		return Style.BRIGHT + Fore.YELLOW + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'g':
		return Style.BRIGHT + Fore.GREEN + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'b':
		return Style.BRIGHT + Fore.BLUE + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'c':
		return Style.BRIGHT + Fore.CYAN + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'r':
		return Style.BRIGHT + Fore.RED + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'de':
		return Style.BRIGHT + Fore.RESET + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)

def get_random(arr):
	return arr[random.randrange(0,len(arr))]

def _select(m):
	choices = m.group(1).split('|')
	return choices[random.randint(0, len(choices)-1)]


def spin(text, tokens=None):
	r = re.compile('{([^{}]*)}')
	while True:
		text, n = r.subn(_select, text)
		if n == 0: break
	if tokens:
		text = multi_replace(text, tokens)
	return text.strip()


def multi_spin(text, tokens=None, delimiter= '\n'):
	lines = text.strip().split(delimiter)
	line = array.get_random(lines)
	return spin(line, tokens)


def multi_replace(text, dic):
	pattern = "|".join(map(re.escape,dic.keys()))
	return re.sub(pattern,lambda m: dic[m.group()],text)

def set_session_proxy(session, proxy, proxy_auth=None):
	if proxy_auth != None:
		session.proxies = {
		'http' : 'http://' + proxy_auth + '@' + proxy,
		'https' : 'http://' + proxy_auth + '@' + proxy
		}
	else:
		session.proxies = {
		'http' : 'http://' + proxy,
		'https' : 'http://' + proxy
		}

def get_spinxo_names():
	# print log('c', 'Getting random username from spinxo...')
	spinxo_headers = {
	'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
	'Accept-Language': 'en-US,en;q=0.5',
	'Accept-Encoding': 'gzip, deflate',
	'Connection': 'keep-alive'
	}
	response = requests.get('http://spinxo.com/', headers = spinxo_headers, verify = False)
	if response.status_code == 200:
		soup = BS(response.content)

		# generate string
		digits = "".join( [random.choice(string.digits) for i in range(random.randint(3,8))] )
		chars = "".join( [random.choice(string.ascii_letters) for i in range(random.randint(4,11))] )
		chars2 = "".join( [random.choice(string.ascii_letters) for i in range(random.randint(4,11))] )

		alphabet = "abcdefghijklmnopqrstuvwxyz"
		upperalphabet = alphabet.upper()
		pw_len = 162
		pwlist = []

		for i in range(pw_len//3):
		    pwlist.append(alphabet[random.randrange(len(alphabet))])
		    pwlist.append(upperalphabet[random.randrange(len(upperalphabet))])
		    pwlist.append(str(random.randrange(10)))
		for i in range(pw_len-len(pwlist)):
		    pwlist.append(alphabet[random.randrange(len(alphabet))])

		random.shuffle(pwlist)
		_notification_id = "".join(pwlist)



		_username = random.choice(list(soup.findAll(attrs={'name' : 'lnkName'}))).text.lower().strip()+'@{0}.com'.format(chars2)
		_password = digits+chars
		print log('g', 'Getting username success.')
		return [_username, _password, _notification_id]
	else:
		print log('r', 'Failed to grab username.')

def get_email():
	digits = "".join( [random.choice(string.digits) for i in range(random.randint(3,8))] )
	chars = "".join( [random.choice(string.ascii_letters) for i in range(random.randint(4,11))] )
	return [digits + '@' + chars + '.com', chars + digits ]

def calculate_est(est_string):
	eastern = pytz.timezone('US/Eastern')
	last_log = datetime.datetime.strptime(est_string.split(' EST')[0], '%m/%d/%Y %H:%M:%S')
	asd = datetime.datetime.now(eastern).replace(tzinfo=None) - last_log
	return asd.seconds

def get_hidden_input(soup):
	login_data = {}
	for _input in soup.findAll('input', attrs={'type' : 'hidden'}):
		try:
			login_data[_input['name']] = _input['value']
		except:
			login_data[_input['name']] = ''
	return login_data