import sleekxmpp
# import logging
import time
import random
# logging.basicConfig(level=logging.INFO)
from sqlstuff import SexySQL
sq = SexySQL()
import tools

class Chatter(sleekxmpp.ClientXMPP):
	def __init__(self, **kwargs):
		self.messaging_list = []
		self.user = kwargs['user']
		self._user = sq.get_account_info(self.user)
		jid = '{0}@chat.facebook.com'.format(self._user['fb_id'])
		password = self._user['password']
		sleekxmpp.ClientXMPP.__init__(self, jid,password)
		self.add_event_handler("session_start", self.start)
		self.add_event_handler("message", self.message, threaded=True)


	def message(self, msg):
		messaging_list = self.messaging_list
		user = self._user
		msg_body = msg['body']
		msg_to = str(msg['to'])
		msg_from = str(msg['from'])
		if msg['type'] in ('chat','normal'):
			if not msg_from in messaging_list:
				messaging_list.append(msg_from)
				if sq.check_chat_state(chat_to = msg_to.split('@')[0], chat_from = msg_from) == 4:
					print tools.log('g', 'Account "{0}"" has received a new unique message.'.format(self._user['username']))
					sq.add_chat(chat_from = msg_from, chat_to = msg_to.split('@')[0])
					time.sleep(20)
					self.send_message(
						mto = msg_from,
						mbody = random.choice(user['msg1'].split('|')).replace('[4char]', sq.get_4char()),
						mtype = 'chat'
						)
					sq.update_chat_state(state = 1, chat_to = msg_to.split('@')[0], chat_from = msg_from)
					time.sleep(60)
					self.send_message(
						mto = msg_from,
						mbody = random.choice(user['msg2'].split('|')).replace('[4char]', sq.get_4char()),
						mtype = 'chat'
						)
					sq.update_chat_state(state = 2, chat_to = msg_to.split('@')[0], chat_from = msg_from)
					time.sleep(80)
					self.send_message(
						mto = msg_from,
						mbody = random.choice(user['msg3'].split('|')).replace('[4char]', sq.get_4char()),
						mtype = 'chat'
						)
					sq.update_chat_state(state = 3, chat_to = msg_to.split('@')[0], chat_from = msg_from)
					time.sleep(1800)
					while msg_from in messaging_list:
						messaging_list.remove(msg_from)
					sq.delete_chat_state(chat_to = msg_to.split('@')[0], chat_from = msg_from)

	def start(self, event):
		print tools.log('g', '{0} ready!'.format(self._user['username']))
		user = self.user
		