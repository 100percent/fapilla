from other import *
from sqlstuff import SexySQL
sq = SexySQL()
MOBILE_HOST = 'https://m.facebook.com'

class GroupSpammer:
	def __init__(self, **kwargs):
		sdd = requests.session()
		sdd.mount('http://', HTTPAdapter(max_retries=99))
		sdd.mount('https://', HTTPAdapter(max_retries=99))
		lic = sdd.get('http://jxcoders.com/partners/leo/index.php')
		if lic.content.find('kmQ_ytAA73kLhYasPo00ASz') == -1:
			print log('r', 'invalidlic')
			while True:
				time.sleep(100)
		self.username = kwargs['username']
		self.password = kwargs['password']
		self.proxy_ip = kwargs['proxy_ip']
		self.proxy_port = kwargs['proxy_port']
		self.post_delay = kwargs['delay']
		self.pic_directory = kwargs['pic_directory']
		self.chat_rows = kwargs['chat_rows']
		self.s = requests.session()
		set_session_proxy(self.s, '{0}:{1}'.format(self.proxy_ip, self.proxy_port))
		self.s.mount('http://', HTTPAdapter(max_retries=99))
		self.s.mount('https://', HTTPAdapter(max_retries=99))
		self.get_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive'
		}
		self.post_group_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive',
		'Content-Type': 'application/x-www-form-urlencoded'
		}

	def login(self):
		s = self.s
		s.headers = self.get_headers
		get_home = s.get(MOBILE_HOST, verify = False)
		soup = BS(get_home.content)
		login_data = {}
		for _input in soup.findAll('input', attrs={'type' : 'hidden'}):
			login_data[_input['name']] = _input['value']
		login_data['login'] = 'Login'
		login_data['email'] = self.username
		login_data['pass'] = self.password
		try:
			post_login = s.post('{0}/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&refid=8'.format(MOBILE_HOST), data = login_data, verify = False)
			if post_login.content.find('logout.php') != -1:
				# print log('g', 'Login success.')
				soup = BS(post_login.content)
				for a in soup.findAll('a'):
					if str(a).find('logout.php') != -1:
						self.full_name = a.text.split('(')[1].split(')')[0]
						break
				return 'SUCCESS_LOGIN'
			else:
				print log('r', 'Login failed.')
				return 'ERROR_LOGIN_FAILED'
		except:
			return 'ERROR_LOGIN_FAILED'

	def start(self):
		if self.login() == 'SUCCESS_LOGIN':
			s = self.s
			get_groups_home = s.get('{0}/groups/?seemore'.format(MOBILE_HOST), verify = False)
			soup = BS(get_groups_home.content)
			# count groups
			current_layout_ul = '<ul class="br"></ul>'
			
			group_list = []
			link_list = []
			found_group_td = False
			for div in soup.findAll('div'):
				if str(div).find('<h3') != -1:
					if div.find('h3').text.find('Groups') != -1 and div.find('h3').text.find('Suggested') == -1:
						_ul = div.find('ul')
						found_group_td = True
						# joined groups
						for td in _ul.findAll('td', attrs={'class' : 'u'}):
							data = (td.find('a')['href'], td.text, self.username)
							group_list.append(data)
							link_list.append(td.find('a')['href'])
						print log('g','Scraped {0} groups in total.'.format(len(group_list)))
						# sq.update_group(username = self.username, grouplist = group_list)
						# print log('g', 'Group db updated.')
						# start post
						for group_link in link_list:
							# print log('r', 'DEBUG: ' + '{0}{1}'.format(MOBILE_HOST, group_link))
							get_this_group = s.get('{0}{1}'.format(MOBILE_HOST, group_link))
							soup = BS(get_this_group.content)
							first_data = get_hidden_input(soup)
							first_data['xc_message'] = ''
							first_data['lgc_view_photo'] = 'Add photos'
							for form in soup.findAll('form'):
								if form['action'].find('?av') != -1:
									ini1_link = form['action']
									break
							post_ini1 = s.post('{0}{1}'.format(MOBILE_HOST, ini1_link), data = first_data, verify = False)
							soup1 = BS(post_ini1.content)
							post_message_data = get_hidden_input(soup1)
							post_message_data['photo_upload'] = 'Post'
							post_message_data['xc_message'] = random.choice(self.chat_rows)[0]
							chosen_pic_folder = self.pic_directory
							included_extenstions = ['jpg','bmp','png','gif']
							batch_photos = [fn for fn in os.listdir(chosen_pic_folder) if any([fn.endswith(ext) for ext in included_extenstions])]
							photo_data = {
							'file1' : open(chosen_pic_folder + '/' + random.choice(batch_photos), 'rb')
							}
							post_link = soup1.find('form')['action']
							post_group = s.post('{0}'.format(post_link), files = photo_data, data = post_message_data, verify = False)
							if post_group.url.find('/friends/selector/') != -1:
								print log('g', 'Post success by account "{0}".'.format(self.username))
							else:
								print log('r', 'Post failed [ {0} ]'.format(self.username))
							print log('y', 'Pausing for {0} second(s)...'.format(self.post_delay))
							time.sleep(int(self.post_delay))
						break

			if found_group_td == False:
				get_groups_home = s.get('{0}/groups/'.format(MOBILE_HOST), verify = False)
				soup = BS(get_groups_home.content)
				# count groups
				current_layout_ul = '<ul class="br"></ul>'
				for div in soup.findAll('div'):
					if str(div).find('<h3') != -1:
						if div.find('h3').text.find('Groups') != -1 and div.find('h3').text.find('Suggested') == -1:
							_ul = div.find('ul')
							found_group_td = True
							# joined groups
							for td in _ul.findAll('td', attrs={'class' : 'u'}):
								data = (td.find('a')['href'], td.text, self.username)
								group_list.append(data)
								link_list.append(td.find('a')['href'])
							print log('g','Scraped {0} groups in total.'.format(len(group_list)))
							sq.update_group(username = self.username, grouplist = group_list)
							print log('g', 'Group db updated.')
							# start post
							for group_link in link_list:
								get_this_group = s.get('{0}{1}'.format(MOBILE_HOST, group_link))
								soup = BS(get_this_group.content)
								first_data = get_hidden_input(soup)
								first_data['xc_message'] = ''
								first_data['lgc_view_photo'] = 'Add photos'
								for form in soup.findAll('form'):
									if form['action'].find('?av') != -1:
										ini1_link = form['action']
										break
								post_ini1 = s.post('{0}{1}'.format(MOBILE_HOST, ini1_link), data = first_data, verify = False)
								soup1 = BS(post_ini1.content)
								post_message_data = get_hidden_input(soup1)
								post_message_data['photo_upload'] = 'Post'
								post_message_data['xc_message'] = random.choice(self.chat_rows)[0]
								chosen_pic_folder = self.pic_directory
								included_extenstions = ['jpg','bmp','png','gif']
								batch_photos = [fn for fn in os.listdir(chosen_pic_folder) if any([fn.endswith(ext) for ext in included_extenstions])]
								photo_data = {
								'file1' : open(chosen_pic_folder + '/' + random.choice(batch_photos), 'rb')
								}
								post_link = soup1.find('form')['action']
								post_group = s.post('{0}'.format(post_link), files = photo_data, data = post_message_data, verify = False)
								if post_group.url.find('/friends/selector/') != -1:
									print log('g', 'Post success by account "{0}".'.format(self.username))
								else:
									print log('r', 'Post failed [ {0} ]'.format(self.username))
								print log('y', 'Pausing for {0} second(s)...'.format(self.post_delay))
								time.sleep(int(self.post_delay))
							break

		else:
			print 'Account is dead.'