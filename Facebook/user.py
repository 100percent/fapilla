import tools
from sqlstuff import SexySQL
from bs4 import BeautifulSoup as BS
MOBILE_HOST = 'https://m.facebook.com'
sq = SexySQL()
class User:
	def __init__(self, __user, **args):
		sdd = tools.new_session()
		lic = sdd.get('http://jxcoders.com/partners/leo')
		if lic.content.find('kmQ_ytAA73kLhYasPo00ASz') == -1:
			print tools.log('r', 'invalidlic')
			while True:
				time.sleep(100)
		# user = sq.get_account_info(__user)
		self.get_headers = tools.get_headers
		self.post_headers = tools.post_headers
		self.max = args['max']
		self.username = __user['username']
		self.password = __user['password']
		self.proxy_ip = __user['proxy_ip']
		self.proxy_port = __user['proxy_port']
		self.s = tools.new_session()
		tools.set_session_proxy(self.s, '{0}:{1}'.format(self.proxy_ip, self.proxy_port))
		self.full_name = ''
		self.fb_id = ''
		if 'add' in args:
			self.add_account(args['thread_no'])


	def login(self):
		s = self.s
		s.headers = self.get_headers
		get_home = s.get(MOBILE_HOST, verify = False)
		soup = BS(get_home.content)
		login_data = {}
		for _input in soup.findAll('input', attrs={'type' : 'hidden'}):
			login_data[_input['name']] = _input['value']
		login_data['login'] = 'Login'
		login_data['email'] = self.username
		login_data['pass'] = self.password
		try:
			post_login = s.post('{0}/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&refid=8'.format(MOBILE_HOST), data = login_data, verify = False)
			if post_login.content.find('logout.php') != -1:
				# print tools.log('g', 'Login success.')
				soup = BS(post_login.content)
				# find full_name
				for a in soup.findAll('a'):
					if str(a).find('logout.php') != -1:
						self.full_name = a.text.split('(')[1].split(')')[0]
						break
				# find fb_id
				get_home = s.get(MOBILE_HOST, verify = False)
				soup = BS(get_home.content)
				self.fb_id = soup.find('input', attrs={'type' : 'hidden', 'name' : 'target'})['value']
				return 'SUCCESS_LOGIN'
			else:
				# print tools.log('r', 'Login failed.')
				return 'ERROR_LOGIN_FAILED'
		except:
			return 'ERROR_LOGIN_FAILED'

	def add_account(self, i):
		login = self.login()
		if str(login) != 'ERROR_LOGIN_FAILED':
			try:
				sq.add_account(username = self.username, password = self.password, fb_id = self.fb_id, full_name = self.full_name, proxy_port = self.proxy_port, proxy_ip = self.proxy_ip)
				print tools.log('g', 'Added {0} to database!'.format(self.full_name))
			except Exception as e:
				print tools.log('r', 'ERROR: {0}'.format(str(e)))
				if str(e).find('is not unique'):
					pass
				else:
					print tools.log('r', 'ERROR: {0}'.format(str(e)))
		ii = i + 1
		if ii == self.max:
			print tools.log('g', 'TASK_DONE.')
			self.task_done = True

	def get_fullname(self):
		return self.full_name