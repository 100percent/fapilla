from other import *
MOBILE_HOST = 'https://m.facebook.com'

class Unfriender:
	def __init__(self, **kwargs):
		self._finished = False
		sdd = requests.session()
		sdd.mount('http://', HTTPAdapter(max_retries=99))
		sdd.mount('https://', HTTPAdapter(max_retries=99))
		lic = sdd.get('http://jxcoders.com/partners/leo/index.php')
		if lic.content.find('kmQ_ytAA73kLhYasPo00ASz') == -1:
			print log('r', 'invalidlic')
			while True:
				time.sleep(100)
		try:
			self._amount = kwargs['amount']
			self.username = kwargs['username']
			self.password = kwargs['password']
			self.proxy_ip = kwargs['proxy_ip']
			self.proxy_port = kwargs['proxy_port']
			self.s = requests.session()
			set_session_proxy(self.s, '{0}:{1}'.format(self.proxy_ip, self.proxy_port))
			self.s.mount('http://', HTTPAdapter(max_retries=99))
			self.s.mount('https://', HTTPAdapter(max_retries=99))
		except:
			pass
		self.deleted_friends = 0
		self.get_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive'
		}
		self.post_group_headers = {
		'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Language': 'en-US,en;q=0.5',
		'Accept-Encoding': 'gzip, deflate',
		'Connection': 'keep-alive',
		'Content-Type': 'application/x-www-form-urlencoded'
		}

	def set_amount(self, a):
		self._amount = a

	def login(self):
		s = self.s
		s.headers = self.get_headers
		get_home = s.get(MOBILE_HOST, verify = False)
		soup = BS(get_home.content)
		login_data = {}
		for _input in soup.findAll('input', attrs={'type' : 'hidden'}):
			login_data[_input['name']] = _input['value']
		login_data['login'] = 'Login'
		login_data['email'] = self.username
		login_data['pass'] = self.password
		try:
			post_login = s.post('{0}/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&refid=8'.format(MOBILE_HOST), data = login_data, verify = False)
			if post_login.content.find('logout.php') != -1:
				print log('g', 'Login success.')
				return 'SUCCESS_LOGIN'
			else:
				print log('r', 'Login failed.')
				return 'ERROR_LOGIN_FAILED'
		except:
			return 'ERROR_LOGIN_FAILED'

	def start(self):
		if self.login() == 'SUCCESS_LOGIN':
			_main_start(self._amount / 10, self.s)
	def unf(self, thread_no, s):
		for i in xrange(10):
			ii = i * thread_no
			fr_count = 0
			friend_list_link = '{0}/friends/center/friends/?ppk={1}'.format(MOBILE_HOST, ii)
			s.headers = self.get_headers
			current_friends_page = s.get(friend_list_link, verify = False)
			soup = BS(current_friends_page.content)
			for a in soup.findAll('a'):
				if a['href'].find('/friends/hovercard/mbasic') != -1:
					try:
						fr_count = fr_count = 1
						current_uid = a['href'].split('/')[4].split('=')[1].split('&')[0]
						# print log('c', 'Deleting friend {0}...'.format(current_uid))

						# friend home_page
						unfriend_home = s.get('{0}/removefriend.php?friend_id={1}&unref=profile_gear&refid=17'.format(MOBILE_HOST, current_uid))
						soup = BS(unfriend_home.content)
						unfriend_data = {
						'fb_dtsg' : soup.find('input', attrs={'type' : 'hidden', 'name' : 'fb_dtsg'})['value'],
						'charset_test' : soup.find('input', attrs={'type' : 'hidden', 'name' : 'charset_test'})['value'],
						'friend_id' : current_uid,
						'unref' : 'profile_gear',
						'confirm' : 'Confirm'
						}
						s.headers = self.post_group_headers
						post_unfriend = s.post('{0}/a/removefriend.php'.format(MOBILE_HOST), data = unfriend_data, verify = False)
						if post_unfriend.content.find('You are no longer friends with') != -1:
							# success unfriend
							self.deleted_friends = self.deleted_friends + 1
							# print log('g', '{0} - {1} removed.'.format(self.username, self.deleted_friends))
							print log('g', 'unfriend_success')
							print ' >>>>>> ' + str(self.deleted_friends)
							if self._amount == self.deleted_friends:
								self._finished = True
								# print log('g', 'Unfriend Module finished.')
								break
					except Exception as e:
						print 'ERROR: ' + str(e)
			#if fr_count == 0 or self._amount == self.deleted_friends:
			if self._amount == self.deleted_friends:
				# no more friends to list
				# print log('g', 'unfriend_done')
				# print log('g', 'Unfriend Module finished.')
				break
uu = Unfriender()
def _main_start(t_a, s):
	manager = ThreadManager()
	print log('g', 'Starting "UNFRIEND MODULE" with {0} threads...'.format(t_a))
	uu.set_amount(t_a * 10)
	manager.start(int(t_a), s)
class ThreadManager:
	def __init__(self):
		pass
		
	def start(self, threads, s):
		thread_refs = []
		for i in range(threads):
			t = MyThread(i, s)
			t.daemon = True
			t.start()
			time.sleep(1)
		for t in thread_refs:
			t.join()
class MyThread(Thread):
	def __init__(self, i, s):
		Thread.__init__(self)
		self.s = s
		self.i = i
		
	def run(self):
		uu.unf(self.i, self.s)