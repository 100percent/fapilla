import argparse
from threading import Thread
import datetime
import time
import sqlite3
import Facebook as fb
import ConfigParser
# output lib
# changed v1
from colorama import init
init(autoreset=True)
from colorama import Fore, Back, Style

# config
config_location = 'fbot.ini'
conf = ConfigParser.ConfigParser()
conf.read(config_location)
accept_friendrequest_delay = conf.get('Delay', 'AcceptFriendRequestDelay')
cycle_delay = conf.get('Delay', 'CycleDelay')
posting_delay = conf.get('Delay', 'PostToGroupsDelay')
pic_directory = conf.get('Directory', 'PostPics')


def log(color, message):
	if color == 'y':
		return Style.BRIGHT + Fore.YELLOW + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'g':
		return Style.BRIGHT + Fore.GREEN + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'b':
		return Style.BRIGHT + Fore.BLUE + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'c':
		return Style.BRIGHT + Fore.CYAN + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'r':
		return Style.BRIGHT + Fore.RED + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)
	elif color == 'de':
		return Style.BRIGHT + Fore.RESET + str(datetime.datetime.now().strftime("%I:%M")) + Fore.RESET + ' ' + str(message)


conn = sqlite3.connect('db/jx.db')
c = conn.cursor()
c.execute('SELECT * from account WHERE active=1')
rows = c.fetchall()
c.execute('SELECT * from groupText')
chat_rows = c.fetchall()
conn.close()

THREADS = len(rows)

def main():
	manager = ThreadManager()
	print log('g', 'Starting "SPAM MODULE" with {0} threads...'.format(THREADS))
	manager.start(int(THREADS))
class ThreadManager:
	def __init__(self):
		pass
		
	def start(self, threads):
		thread_refs = []
		for i in range(threads):
			t = MyThread(i)
			t.daemon = True
			t.start()
			time.sleep(1)
		for t in thread_refs:
			t.join()
class MyThread(Thread):
	def __init__(self, i):
		Thread.__init__(self)
		self.i = i
		
	def run(self):
		i = self.i
		r = rows[i]
		while True:
			spammer = fb.GroupSpammer(
				username = r[0],
				password = r[1],
				proxy_ip = r[3],
				proxy_port = r[4],
				delay = posting_delay,
				pic_directory = pic_directory,
				chat_rows = chat_rows
				)
			spammer.start()
			# user = fb.User([cl[0], cl[1], cl[2], cl[3].split('\n')[0]], add = True, max = THREADS, thread_no = i)
			print log('g', 'Account "{0}" has finished posting to all groups. Sleeping for {1}...'.format(r[0], cycle_delay))
			time.sleep(int(cycle_delay))
main()
while True:
	time.sleep(1)